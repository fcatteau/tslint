package convert

import (
	"reflect"
	"strings"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
)

func TestConvert(t *testing.T) {
	in := `[
		{
			"endPosition": {
				"character": 12,
				"line": 3,
				"position": 74
			},
			"failure": "eval with argument of type Identifier",
			"name": "src/main.ts",
			"ruleName": "tsr-detect-eval-with-expression",
			"ruleSeverity": "ERROR",
			"startPosition": {
				"character": 0,
				"line": 3,
				"position": 62
			}
		},
		{
			"endPosition": {
				"character": 39,
				"line": 14,
				"position": 393
			},
			"failure": "Found non-literal argument to RegExp Constructor",
			"name": "src/main.ts",
			"ruleName": "tsr-detect-non-literal-regexp",
			"ruleSeverity": "ERROR",
			"startPosition": {
				"character": 15,
				"line": 14,
				"position": 369
			}
		},
		{
			"endPosition": {
				"character": 12,
				"line": 5,
				"position": 84
			},
			"failure": "Found non-literal argument in require",
			"name": "src/main.ts",
			"ruleName": "tsr-detect-non-literal-require",
			"ruleSeverity": "ERROR",
			"startPosition": {
				"character": 0,
				"line": 5,
				"position": 72
			}
		},
		{
			"endPosition": {
				"character": 21,
				"line": 29,
				"position": 833
			},
			"failure": "Potential timing attack on the right side of expression",
			"name": "src/main.ts",
			"ruleName": "tsr-detect-possible-timing-attacks",
			"ruleSeverity": "ERROR",
			"startPosition": {
				"character": 4,
				"line": 29,
				"position": 816
			}
		},
    {
			"endPosition": {
				"character": 1,
				"line": 0,
				"position": 1
			},
			"failure": "Disallows trailing whitespace at the end of a line.",
			"name": "src/main.ts",
			"ruleName": "no-trailing-whitespace",
			"ruleSeverity": "WARNING",
			"startPosition": {
				"character": 0,
				"line": 0,
				"position": 0
			}
    }
	]`

	var scanner = issue.Scanner{
		ID:   scannerID,
		Name: scannerName,
	}

	r := strings.NewReader(in)
	want := &issue.Report{
		Version: issue.CurrentVersion(),
		Vulnerabilities: []issue.Issue{
			{
				Category:    issue.CategorySast,
				Scanner:     scanner,
				Name:        "eval with argument of type Identifier",
				Message:     "eval with argument of type Identifier",
				Description: "Detects `eval(variable)` which can allow an attacker to run arbitary code inside your process.",
				CompareKey:  "src/main.ts:4:tsr-detect-eval-with-expression",
				Severity:    issue.SeverityLevelUnknown,
				Confidence:  issue.ConfidenceLevelUnknown,
				Location: issue.Location{
					File:      "src/main.ts",
					LineStart: 4,
					LineEnd:   4,
				},
				Identifiers: []issue.Identifier{
					{
						Type:  "tslint_rule_id",
						Name:  "TSLint rule ID tsr-detect-eval-with-expression",
						Value: "tsr-detect-eval-with-expression",
						URL:   "https://github.com/webschik/tslint-config-security#tsr-detect-eval-with-expression",
					},
				},
			},
			{
				Category:    issue.CategorySast,
				Scanner:     scanner,
				Name:        "Found non-literal argument to RegExp Constructor",
				Message:     "Found non-literal argument to RegExp Constructor",
				Description: "Detects `RegExp(variable)`, which might allow an attacker to DOS your server with a long-running regular expression.",
				CompareKey:  "src/main.ts:15:tsr-detect-non-literal-regexp",
				Severity:    issue.SeverityLevelUnknown,
				Confidence:  issue.ConfidenceLevelUnknown,
				Location: issue.Location{
					File:      "src/main.ts",
					LineStart: 15,
					LineEnd:   15,
				},
				Identifiers: []issue.Identifier{
					{
						Type:  "tslint_rule_id",
						Name:  "TSLint rule ID tsr-detect-non-literal-regexp",
						Value: "tsr-detect-non-literal-regexp",
						URL:   "https://github.com/webschik/tslint-config-security#tsr-detect-non-literal-regexp",
					},
				},
			},
			{
				Category:    issue.CategorySast,
				Scanner:     scanner,
				Name:        "Found non-literal argument in require",
				Message:     "Found non-literal argument in require",
				Description: "Detects `require(variable)`, which might allow an attacker to load and run arbitrary code, or access arbitrary files on disk.",
				CompareKey:  "src/main.ts:6:tsr-detect-non-literal-require",
				Severity:    issue.SeverityLevelUnknown,
				Confidence:  issue.ConfidenceLevelUnknown,
				Location: issue.Location{
					File:      "src/main.ts",
					LineStart: 6,
					LineEnd:   6,
				},
				Identifiers: []issue.Identifier{
					{
						Type:  "tslint_rule_id",
						Name:  "TSLint rule ID tsr-detect-non-literal-require",
						Value: "tsr-detect-non-literal-require",
						URL:   "https://github.com/webschik/tslint-config-security#tsr-detect-non-literal-require",
					},
				},
			},

			{
				Category:    issue.CategorySast,
				Scanner:     scanner,
				Name:        "Potential timing attack on the right side of expression",
				Message:     "Potential timing attack on the right side of expression",
				Description: "Detects insecure comparisons (==, !=, !== and ===), which check input sequentially.",
				CompareKey:  "src/main.ts:30:tsr-detect-possible-timing-attacks",
				Severity:    issue.SeverityLevelUnknown,
				Confidence:  issue.ConfidenceLevelUnknown,
				Location: issue.Location{
					File:      "src/main.ts",
					LineStart: 30,
					LineEnd:   30,
				},
				Identifiers: []issue.Identifier{
					{
						Type:  "tslint_rule_id",
						Name:  "TSLint rule ID tsr-detect-possible-timing-attacks",
						Value: "tsr-detect-possible-timing-attacks",
						URL:   "https://github.com/webschik/tslint-config-security#tsr-detect-possible-timing-attacks",
					},
				},
			},
		},
		DependencyFiles: []issue.DependencyFile{},
		Remediations:    []issue.Remediation{},
	}
	got, err := Convert(r, "")
	if err != nil {
		t.Fatal(err)
	}
	if !reflect.DeepEqual(want, got) {
		t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
	}
}
