FROM golang:1.13 AS build
ENV CGO_ENABLED=0 GOOS=linux
WORKDIR /go/src/app
COPY . .
RUN go build -o analyzer

FROM node:11-alpine

ARG TS_VERSION
ARG TSLINT_VERSION
ARG TSLINT_SECURITY_VERSION

ENV TS_VERSION ${TS_VERSION:-3.9.2}
ENV TSLINT_VERSION ${TSLINT_VERSION:-6.1.2}
ENV TSLINT_SECURITY_VERSION ${TSLINT_SECURITY_VERSION:-1.16.0}

# The node user below doesn't have permission to create a file in /etc/ssl/certs, this
# RUN command creates a file that the analyzer can add additional ca certs to trust.
RUN mkdir -p /etc/ssl/certs/ && \
    touch /etc/ssl/certs/ca-cert-additional-gitlab-bundle.pem && \
    chown root:node /etc/ssl/certs/ca-cert-additional-gitlab-bundle.pem && \
    chmod g+w /etc/ssl/certs/ca-cert-additional-gitlab-bundle.pem

# --unsafe-perm is a needed workaround for  https://github.com/npm/uid-number/issues/7
# Or else it doesn't build on gitlab-runner
RUN npm install -g \
  --unsafe-perm \
  typescript@$TS_VERSION \
  tslib \
  tslint@$TSLINT_VERSION \
  tslint-config-security@$TSLINT_SECURITY_VERSION

USER node
WORKDIR /home/node

COPY --from=build --chown=root:root /go/src/app/analyzer /
COPY tslint.json /home/node/tslint.json

ENTRYPOINT []
CMD ["/analyzer", "run"]

