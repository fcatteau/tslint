package plugin

import (
	"os"
	"path/filepath"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/plugin"
)

// Match checks if the file extension of info is .ts or .tsx.
func Match(path string, info os.FileInfo) (bool, error) {
	switch filepath.Ext(info.Name()) {
	case ".ts", ".tsx":
		return true, nil
	default:
		return false, nil
	}
}

func init() {
	plugin.Register("tslint", Match)
}
