package main

import (
	"bytes"
	"io"
	"io/ioutil"
	"os"
	"os/exec"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli"
)

const (
	pathTSLint   = "tslint"
	pathTSLintRC = "/home/node/tslint.json"
)

func analyzeFlags() []cli.Flag {
	return []cli.Flag{}
}

func analyze(c *cli.Context, path string) (io.ReadCloser, error) {
	// Perform the analysis
	cmd := exec.Command(pathTSLint, "-c", pathTSLintRC, "-t", "json", "./**/*.ts", "./**/*.tsx")
	cmd.Dir = path
	cmd.Stderr = os.Stderr

	output, err := cmd.Output()
	log.Debugf("%s\n%s", cmd.String(), output)
	if err != nil {
		// An error occurred while running TSLint
		// (see https://palantir.github.io/tslint/usage/cli/#exit-codes )
		log.Errorf("An error occurred while running tslint:\n%s", err)
		return nil, err
	}

	return ioutil.NopCloser(bytes.NewReader(output)), nil
}
