# TSLint analyzer changelog

## v2.4.0
- Switch to the MIT Expat license (!19)

## v2.3.0
- Update logging to be standardized across analyzers (!17)

## v2.2.2
- Remove location.dependency from the generated SAST report (!16)

## v2.2.1
- Update tslint to 6.1.2
- Update typescript to 3.9.2

## v2.2.0
- Add `id` field to vulnerabilities in JSON report (!12)

## v2.1.0
- Add support for custom CA certs (!10)

## v2.0.2
- Update tslint version to 5.20.0
- Update tslint-config-security to 1.16.0

## v2.0.1
- Bump common to v2.1.6

## v2.0.0
- Initial release
